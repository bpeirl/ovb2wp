//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.7 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2014.02.04 � 12:11:55 AM CET 
//


package bp.ovb2wp.ovb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}published_at"/>
 *         &lt;element ref="{}status"/>
 *         &lt;element ref="{}author_name"/>
 *         &lt;element ref="{}author_email"/>
 *         &lt;element ref="{}author_url"/>
 *         &lt;element ref="{}author_ip"/>
 *         &lt;element ref="{}content"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "publishedAt",
    "status",
    "authorName",
    "authorEmail",
    "authorUrl",
    "authorIp",
    "content"
})
@XmlRootElement(name = "comment")
public class Comment {

    @XmlElement(name = "published_at", required = true)
    protected String publishedAt;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(name = "author_name", required = true)
    protected String authorName;
    @XmlElement(name = "author_email", required = true)
    protected String authorEmail;
    @XmlElement(name = "author_url", required = true)
    protected String authorUrl;
    @XmlElement(name = "author_ip", required = true)
    protected String authorIp;
    @XmlElement(required = true)
    protected String content;

    /**
     * Obtient la valeur de la propri�t� publishedAt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPublishedAt() {
        return publishedAt;
    }

    /**
     * D�finit la valeur de la propri�t� publishedAt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPublishedAt(String value) {
        this.publishedAt = value;
    }

    /**
     * Obtient la valeur de la propri�t� status.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * D�finit la valeur de la propri�t� status.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Obtient la valeur de la propri�t� authorName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     * D�finit la valeur de la propri�t� authorName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorName(String value) {
        this.authorName = value;
    }

    /**
     * Obtient la valeur de la propri�t� authorEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorEmail() {
        return authorEmail;
    }

    /**
     * D�finit la valeur de la propri�t� authorEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorEmail(String value) {
        this.authorEmail = value;
    }

    /**
     * Obtient la valeur de la propri�t� authorUrl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorUrl() {
        return authorUrl;
    }

    /**
     * D�finit la valeur de la propri�t� authorUrl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorUrl(String value) {
        this.authorUrl = value;
    }

    /**
     * Obtient la valeur de la propri�t� authorIp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorIp() {
        return authorIp;
    }

    /**
     * D�finit la valeur de la propri�t� authorIp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorIp(String value) {
        this.authorIp = value;
    }

    /**
     * Obtient la valeur de la propri�t� content.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContent() {
        return content;
    }

    /**
     * D�finit la valeur de la propri�t� content.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContent(String value) {
        this.content = value;
    }

}
