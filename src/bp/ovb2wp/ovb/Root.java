//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.7 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2014.02.04 � 12:11:55 AM CET 
//


package bp.ovb2wp.ovb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}blog" maxOccurs="unbounded"/>
 *         &lt;element ref="{}posts"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "blog",
    "posts"
})
@XmlRootElement(name = "root")
public class Root {

    @XmlElement(required = true)
    protected List<Blog> blog;
    @XmlElement(required = true)
    protected Posts posts;

    /**
     * Gets the value of the blog property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the blog property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBlog().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Blog }
     * 
     * 
     */
    public List<Blog> getBlog() {
        if (blog == null) {
            blog = new ArrayList<Blog>();
        }
        return this.blog;
    }

    /**
     * Obtient la valeur de la propri�t� posts.
     * 
     * @return
     *     possible object is
     *     {@link Posts }
     *     
     */
    public Posts getPosts() {
        return posts;
    }

    /**
     * D�finit la valeur de la propri�t� posts.
     * 
     * @param value
     *     allowed object is
     *     {@link Posts }
     *     
     */
    public void setPosts(Posts value) {
        this.posts = value;
    }

}
