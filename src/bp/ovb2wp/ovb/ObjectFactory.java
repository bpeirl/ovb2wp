//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.7 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2014.02.04 � 12:11:55 AM CET 
//


package bp.ovb2wp.ovb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bp.ovb2wp.ovb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Status_QNAME = new QName("", "status");
    private final static QName _Tag_QNAME = new QName("", "tag");
    private final static QName _AuthorUrl_QNAME = new QName("", "author_url");
    private final static QName _Origin_QNAME = new QName("", "origin");
    private final static QName _AuthorIp_QNAME = new QName("", "author_ip");
    private final static QName _PublishedAt_QNAME = new QName("", "published_at");
    private final static QName _Content_QNAME = new QName("", "content");
    private final static QName _Author_QNAME = new QName("", "author");
    private final static QName _Title_QNAME = new QName("", "title");
    private final static QName _AuthorEmail_QNAME = new QName("", "author_email");
    private final static QName _Description_QNAME = new QName("", "description");
    private final static QName _Name_QNAME = new QName("", "name");
    private final static QName _AuthorName_QNAME = new QName("", "author_name");
    private final static QName _CreatedAt_QNAME = new QName("", "created_at");
    private final static QName _Slug_QNAME = new QName("", "slug");
    private final static QName _MainHost_QNAME = new QName("", "main_host");
    private final static QName _ModifiedAt_QNAME = new QName("", "modified_at");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bp.ovb2wp.ovb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Tags }
     * 
     */
    public Tags createTags() {
        return new Tags();
    }

    /**
     * Create an instance of {@link Post }
     * 
     */
    public Post createPost() {
        return new Post();
    }

    /**
     * Create an instance of {@link Comments }
     * 
     */
    public Comments createComments() {
        return new Comments();
    }

    /**
     * Create an instance of {@link Comment }
     * 
     */
    public Comment createComment() {
        return new Comment();
    }

    /**
     * Create an instance of {@link Root }
     * 
     */
    public Root createRoot() {
        return new Root();
    }

    /**
     * Create an instance of {@link Blog }
     * 
     */
    public Blog createBlog() {
        return new Blog();
    }

    /**
     * Create an instance of {@link Posts }
     * 
     */
    public Posts createPosts() {
        return new Posts();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "status")
    public JAXBElement<String> createStatus(String value) {
        return new JAXBElement<String>(_Status_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "tag")
    public JAXBElement<String> createTag(String value) {
        return new JAXBElement<String>(_Tag_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "author_url")
    public JAXBElement<String> createAuthorUrl(String value) {
        return new JAXBElement<String>(_AuthorUrl_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "origin")
    public JAXBElement<String> createOrigin(String value) {
        return new JAXBElement<String>(_Origin_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "author_ip")
    public JAXBElement<String> createAuthorIp(String value) {
        return new JAXBElement<String>(_AuthorIp_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "published_at")
    public JAXBElement<String> createPublishedAt(String value) {
        return new JAXBElement<String>(_PublishedAt_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "content")
    public JAXBElement<String> createContent(String value) {
        return new JAXBElement<String>(_Content_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "author")
    public JAXBElement<String> createAuthor(String value) {
        return new JAXBElement<String>(_Author_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "title")
    public JAXBElement<String> createTitle(String value) {
        return new JAXBElement<String>(_Title_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "author_email")
    public JAXBElement<String> createAuthorEmail(String value) {
        return new JAXBElement<String>(_AuthorEmail_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "description")
    public JAXBElement<String> createDescription(String value) {
        return new JAXBElement<String>(_Description_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "name")
    public JAXBElement<String> createName(String value) {
        return new JAXBElement<String>(_Name_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "author_name")
    public JAXBElement<String> createAuthorName(String value) {
        return new JAXBElement<String>(_AuthorName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "created_at")
    public JAXBElement<XMLGregorianCalendar> createCreatedAt(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_CreatedAt_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "slug")
    public JAXBElement<String> createSlug(String value) {
        return new JAXBElement<String>(_Slug_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "main_host")
    public JAXBElement<String> createMainHost(String value) {
        return new JAXBElement<String>(_MainHost_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "modified_at")
    public JAXBElement<XMLGregorianCalendar> createModifiedAt(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ModifiedAt_QNAME, XMLGregorianCalendar.class, null, value);
    }

}
