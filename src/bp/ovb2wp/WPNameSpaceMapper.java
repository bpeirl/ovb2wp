package bp.ovb2wp;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class WPNameSpaceMapper extends NamespacePrefixMapper
{

	@Override
	public String getPreferredPrefix(String arg0, String arg1, boolean arg2)
	{
		if (arg0.equals("http://wordpress.org/export/1.0/"))
		{
			return "wp";
		}
		else if (arg0.equals("http://purl.org/dc/elements/1.1/"))
		{
			return "dc";
		}
		else if (arg0.equals("http://purl.org/rss/1.0/modules/content/"))
		{
			return "content";			
		}
		else
			return arg1;
	}
	
	/*@Override
    public String[] getPreDeclaredNamespaceUris() {
        return new String[] { "http://wordpress.org/export/1.0/" };
    }*/

}
