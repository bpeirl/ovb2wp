package bp.ovb2wp;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.xml.bind.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

import bp.ovb2wp.ovb.Root;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.util.List;



public class ovb2wp {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ovb2wp window = new ovb2wp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ovb2wp() {
		initialize();
	}
	
	private void testOvb()
	{
		try 
		{
		JAXBContext jc;
		jc = JAXBContext.newInstance("bp.ovb2wp.ovb");
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		Root rt = (Root) unmarshaller.unmarshal(new File("ovb.xml"));
					

		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));					
		marshaller.marshal(rt, System.out);
		}
		catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void testwp()
	{
		try 
		{
		JAXBContext jc;
		jc = JAXBContext.newInstance("bp.ovb2wp.wp");
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		bp.ovb2wp.wp.Rss rt = (bp.ovb2wp.wp.Rss) unmarshaller.unmarshal(new File("wp.xml"));
					

		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));					
		marshaller.marshal(rt, System.out);
		}
		catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private CDataXMLStreamWriter CreateXmlStreamWr(OutputStream s) throws XMLStreamException
	{		
		XMLOutputFactory xof = XMLOutputFactory.newInstance();			
		XMLStreamWriter sw = xof.createXMLStreamWriter(s);
		CDataXMLStreamWriter cds=new CDataXMLStreamWriter(sw);
		
		return cds;
		//new OutputStreamWriter((XMLStreamWriter)cds);
		//return new new Stream(cds);
		//return s;
	}
	
	private void ConvertOvb2WP(String sFileOVB,String sFileWP)
	{
		try
		{
			JAXBContext jc1;
			jc1 = JAXBContext.newInstance("bp.ovb2wp.ovb");
			Unmarshaller unmarshaller = jc1.createUnmarshaller();
			bp.ovb2wp.ovb.Root rt = (bp.ovb2wp.ovb.Root) unmarshaller.unmarshal(new File(sFileOVB));
			bp.ovb2wp.ovb.Blog b=rt.getBlog().get(0);
			
			bp.ovb2wp.wp.ObjectFactory of2 = new bp.ovb2wp.wp.ObjectFactory();
			bp.ovb2wp.wp.Rss rss=of2.createRss();
			rss.setVersion(new BigDecimal("2.0"));
			rss.setChannel(of2.createRssChannel());
			
			bp.ovb2wp.wp.Rss.Channel c=rss.getChannel();			
			c.setTitle(b.getName());
			c.setDescription(b.getDescription());
			c.setPubDate(b.getCreatedAt().toString());
			c.setBaseBlogUrl(b.getMainHost());
			c.setWxrVersion(new BigDecimal("1.0"));

			List<bp.ovb2wp.wp.Rss.Channel.Item> li=c.getItem();
			for (bp.ovb2wp.ovb.Post p:rt.getPosts().getPost())
			{
				bp.ovb2wp.wp.Rss.Channel.Item i=of2.createRssChannelItem();
				li.add(i);
				i.setTitle(p.getTitle());
				i.setPostDate(p.getCreatedAt().toString());
				i.setPubDate(p.getPublishedAt());
				i.setCreator(p.getAuthor());
				String content=p.getContent();
				//content="<![CDATA[Ceci est <B>Un essai!<B>]]>";
				//content=content.replace("![CDATA[","![CDDATA[");
							
				content=content.replace("�","&eacute;");
				content=content.replace("�","&egrave;");
				content=content.replace("�","&ecirc;");
				content=content.replace("�","&euml;");
				content=content.replace("'","&#145;");
				
				i.setEncoded(content);
				
				
				///<![CDDATA[   ]]>
				///<![CDATA[ ]]>
			}
						
			JAXBContext jc2;
			jc2 = JAXBContext.newInstance("bp.ovb2wp.wp");
			Marshaller marshaller = jc2.createMarshaller();			
			
			
			NamespacePrefixMapper mapper = new WPNameSpaceMapper();  
			marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", mapper);			
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));
			marshaller.setProperty("jaxb.encoding", "UTF-8");
			
			CDataXMLStreamWriter sw=CreateXmlStreamWr(new FileOutputStream(new File(sFileWP)));						
			marshaller.marshal(rss, sw);
			sw.flush();
			sw.close();
			
			sw=CreateXmlStreamWr(System.out);						
			marshaller.marshal(rss, sw);
			sw.flush();
			sw.close();
		}
		catch (JAXBException e) 
		{
			e.printStackTrace();
		}	
		catch (Exception e)
		{
			e.printStackTrace();			
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
					//testOvb();
					//testwp();
					ConvertOvb2WP("ovb.xml","out.xml");
					
					
					/*ObjectFactory objFactory = new ObjectFactory();
					Root r = (Root) objFactory.createRoot();
					List<Blog> lb=r.getBlog();
					
					
					Blog b=(Blog)objFactory.createBlog();
					b.setName("Blog de paul");					
					lb.add(b);
								
					JAXBContext jc = JAXBContext.newInstance("bp.ovb2wp");
					Marshaller marshaller = jc.createMarshaller();
					marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, new Boolean(true));					
					marshaller.marshal(r, System.out);*/
					
					
					
				
			}
		});
		frame.getContentPane().add(btnNewButton, BorderLayout.NORTH);
	}

}
