//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.7 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2014.02.06 � 12:04:00 AM CET 
//


package bp.ovb2wp.wp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="comment_id" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="comment_author" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comment_author_email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comment_author_url" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comment_author_IP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comment_date" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comment_date_gmt" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comment_content" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comment_approved" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="comment_type" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="comment_parent" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="comment_user_id" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "commentId",
    "commentAuthor",
    "commentAuthorEmail",
    "commentAuthorUrl",
    "commentAuthorIP",
    "commentDate",
    "commentDateGmt",
    "commentContent",
    "commentApproved",
    "commentType",
    "commentParent",
    "commentUserId"
})
@XmlRootElement(name = "comment")
public class Comment {

    @XmlElement(name = "comment_id")
    @XmlSchemaType(name = "unsignedByte")
    protected short commentId;
    @XmlElement(name = "comment_author", required = true)
    protected String commentAuthor;
    @XmlElement(name = "comment_author_email", required = true)
    protected String commentAuthorEmail;
    @XmlElement(name = "comment_author_url", required = true)
    protected String commentAuthorUrl;
    @XmlElement(name = "comment_author_IP", required = true)
    protected String commentAuthorIP;
    @XmlElement(name = "comment_date", required = true)
    protected String commentDate;
    @XmlElement(name = "comment_date_gmt", required = true)
    protected String commentDateGmt;
    @XmlElement(name = "comment_content", required = true)
    protected String commentContent;
    @XmlElement(name = "comment_approved")
    @XmlSchemaType(name = "unsignedByte")
    protected short commentApproved;
    @XmlElement(name = "comment_type", required = true)
    protected Object commentType;
    @XmlElement(name = "comment_parent")
    @XmlSchemaType(name = "unsignedByte")
    protected short commentParent;
    @XmlElement(name = "comment_user_id")
    @XmlSchemaType(name = "unsignedByte")
    protected short commentUserId;

    /**
     * Obtient la valeur de la propri�t� commentId.
     * 
     */
    public short getCommentId() {
        return commentId;
    }

    /**
     * D�finit la valeur de la propri�t� commentId.
     * 
     */
    public void setCommentId(short value) {
        this.commentId = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentAuthor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommentAuthor() {
        return commentAuthor;
    }

    /**
     * D�finit la valeur de la propri�t� commentAuthor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommentAuthor(String value) {
        this.commentAuthor = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentAuthorEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommentAuthorEmail() {
        return commentAuthorEmail;
    }

    /**
     * D�finit la valeur de la propri�t� commentAuthorEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommentAuthorEmail(String value) {
        this.commentAuthorEmail = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentAuthorUrl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommentAuthorUrl() {
        return commentAuthorUrl;
    }

    /**
     * D�finit la valeur de la propri�t� commentAuthorUrl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommentAuthorUrl(String value) {
        this.commentAuthorUrl = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentAuthorIP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommentAuthorIP() {
        return commentAuthorIP;
    }

    /**
     * D�finit la valeur de la propri�t� commentAuthorIP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommentAuthorIP(String value) {
        this.commentAuthorIP = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommentDate() {
        return commentDate;
    }

    /**
     * D�finit la valeur de la propri�t� commentDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommentDate(String value) {
        this.commentDate = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentDateGmt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommentDateGmt() {
        return commentDateGmt;
    }

    /**
     * D�finit la valeur de la propri�t� commentDateGmt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommentDateGmt(String value) {
        this.commentDateGmt = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentContent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommentContent() {
        return commentContent;
    }

    /**
     * D�finit la valeur de la propri�t� commentContent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommentContent(String value) {
        this.commentContent = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentApproved.
     * 
     */
    public short getCommentApproved() {
        return commentApproved;
    }

    /**
     * D�finit la valeur de la propri�t� commentApproved.
     * 
     */
    public void setCommentApproved(short value) {
        this.commentApproved = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentType.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCommentType() {
        return commentType;
    }

    /**
     * D�finit la valeur de la propri�t� commentType.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCommentType(Object value) {
        this.commentType = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentParent.
     * 
     */
    public short getCommentParent() {
        return commentParent;
    }

    /**
     * D�finit la valeur de la propri�t� commentParent.
     * 
     */
    public void setCommentParent(short value) {
        this.commentParent = value;
    }

    /**
     * Obtient la valeur de la propri�t� commentUserId.
     * 
     */
    public short getCommentUserId() {
        return commentUserId;
    }

    /**
     * D�finit la valeur de la propri�t� commentUserId.
     * 
     */
    public void setCommentUserId(short value) {
        this.commentUserId = value;
    }

}
