//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.7 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2014.02.06 � 12:04:00 AM CET 
//


package bp.ovb2wp.wp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tag_slug" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tag_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tagSlug",
    "tagName"
})
@XmlRootElement(name = "tag")
public class Tag {

    @XmlElement(name = "tag_slug", required = true)
    protected String tagSlug;
    @XmlElement(name = "tag_name", required = true)
    protected String tagName;

    /**
     * Obtient la valeur de la propri�t� tagSlug.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagSlug() {
        return tagSlug;
    }

    /**
     * D�finit la valeur de la propri�t� tagSlug.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagSlug(String value) {
        this.tagSlug = value;
    }

    /**
     * Obtient la valeur de la propri�t� tagName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * D�finit la valeur de la propri�t� tagName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTagName(String value) {
        this.tagName = value;
    }

}
