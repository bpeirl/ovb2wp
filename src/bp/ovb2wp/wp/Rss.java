//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.7 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2014.02.06 � 12:04:00 AM CET 
//


package bp.ovb2wp.wp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="channel">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="link" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="pubDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="generator" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element ref="{http://wordpress.org/export/1.0/}wxr_version"/>
 *                   &lt;element ref="{http://wordpress.org/export/1.0/}base_site_url"/>
 *                   &lt;element ref="{http://wordpress.org/export/1.0/}base_blog_url"/>
 *                   &lt;element ref="{http://wordpress.org/export/1.0/}category" maxOccurs="unbounded"/>
 *                   &lt;element ref="{http://wordpress.org/export/1.0/}tag" maxOccurs="unbounded"/>
 *                   &lt;element name="item" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="link" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="pubDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element ref="{http://purl.org/dc/elements/1.1/}creator"/>
 *                             &lt;element name="category" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;simpleContent>
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                     &lt;attribute name="domain" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                     &lt;attribute name="nicename" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                                   &lt;/extension>
 *                                 &lt;/simpleContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="guid">
 *                               &lt;complexType>
 *                                 &lt;simpleContent>
 *                                   &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                                     &lt;attribute name="isPermaLink" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *                                   &lt;/extension>
 *                                 &lt;/simpleContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element ref="{http://purl.org/rss/1.0/modules/content/}encoded"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}post_id"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}post_date"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}post_date_gmt"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}comment_status"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}ping_status"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}post_name"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}status"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}post_parent"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}menu_order"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}post_type"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}post_password"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}postmeta" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;element ref="{http://wordpress.org/export/1.0/}comment" maxOccurs="unbounded" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="version" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "channel"
})
@XmlRootElement(name = "rss", namespace = "")
public class Rss {

    @XmlElement(namespace = "", required = true)
    protected Rss.Channel channel;
    @XmlAttribute(name = "version", required = true)
    protected BigDecimal version;

    /**
     * Obtient la valeur de la propri�t� channel.
     * 
     * @return
     *     possible object is
     *     {@link Rss.Channel }
     *     
     */
    public Rss.Channel getChannel() {
        return channel;
    }

    /**
     * D�finit la valeur de la propri�t� channel.
     * 
     * @param value
     *     allowed object is
     *     {@link Rss.Channel }
     *     
     */
    public void setChannel(Rss.Channel value) {
        this.channel = value;
    }

    /**
     * Obtient la valeur de la propri�t� version.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVersion() {
        return version;
    }

    /**
     * D�finit la valeur de la propri�t� version.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVersion(BigDecimal value) {
        this.version = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="link" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="pubDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="generator" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element ref="{http://wordpress.org/export/1.0/}wxr_version"/>
     *         &lt;element ref="{http://wordpress.org/export/1.0/}base_site_url"/>
     *         &lt;element ref="{http://wordpress.org/export/1.0/}base_blog_url"/>
     *         &lt;element ref="{http://wordpress.org/export/1.0/}category" maxOccurs="unbounded"/>
     *         &lt;element ref="{http://wordpress.org/export/1.0/}tag" maxOccurs="unbounded"/>
     *         &lt;element name="item" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="link" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="pubDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element ref="{http://purl.org/dc/elements/1.1/}creator"/>
     *                   &lt;element name="category" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;simpleContent>
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                           &lt;attribute name="domain" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                           &lt;attribute name="nicename" type="{http://www.w3.org/2001/XMLSchema}string" />
     *                         &lt;/extension>
     *                       &lt;/simpleContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="guid">
     *                     &lt;complexType>
     *                       &lt;simpleContent>
     *                         &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *                           &lt;attribute name="isPermaLink" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
     *                         &lt;/extension>
     *                       &lt;/simpleContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element ref="{http://purl.org/rss/1.0/modules/content/}encoded"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}post_id"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}post_date"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}post_date_gmt"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}comment_status"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}ping_status"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}post_name"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}status"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}post_parent"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}menu_order"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}post_type"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}post_password"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}postmeta" maxOccurs="unbounded" minOccurs="0"/>
     *                   &lt;element ref="{http://wordpress.org/export/1.0/}comment" maxOccurs="unbounded" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "title",
        "link",
        "description",
        "pubDate",
        "generator",
        "language",
        "wxrVersion",
        "baseSiteUrl",
        "baseBlogUrl",
        "category",
        "tag",
        "item"
    })
    public static class Channel {

        @XmlElement(namespace = "", required = true)
        protected String title;
        @XmlElement(namespace = "", required = true)
        protected String link;
        @XmlElement(namespace = "", required = true)
        protected String description;
        @XmlElement(namespace = "", required = true)
        protected String pubDate;
        @XmlElement(namespace = "", required = true)
        protected String generator;
        @XmlElement(namespace = "", required = true)
        protected String language;
        @XmlElement(name = "wxr_version", required = true)
        protected BigDecimal wxrVersion;
        @XmlElement(name = "base_site_url", required = true)
        protected String baseSiteUrl;
        @XmlElement(name = "base_blog_url", required = true)
        protected String baseBlogUrl;
        @XmlElement(required = true)
        protected List<bp.ovb2wp.wp.Category> category;
        @XmlElement(required = true)
        protected List<Tag> tag;
        @XmlElement(namespace = "", required = true)
        protected List<Rss.Channel.Item> item;

        /**
         * Obtient la valeur de la propri�t� title.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTitle() {
            return title;
        }

        /**
         * D�finit la valeur de la propri�t� title.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTitle(String value) {
            this.title = value;
        }

        /**
         * Obtient la valeur de la propri�t� link.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLink() {
            return link;
        }

        /**
         * D�finit la valeur de la propri�t� link.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLink(String value) {
            this.link = value;
        }

        /**
         * Obtient la valeur de la propri�t� description.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * D�finit la valeur de la propri�t� description.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Obtient la valeur de la propri�t� pubDate.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPubDate() {
            return pubDate;
        }

        /**
         * D�finit la valeur de la propri�t� pubDate.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPubDate(String value) {
            this.pubDate = value;
        }

        /**
         * Obtient la valeur de la propri�t� generator.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGenerator() {
            return generator;
        }

        /**
         * D�finit la valeur de la propri�t� generator.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGenerator(String value) {
            this.generator = value;
        }

        /**
         * Obtient la valeur de la propri�t� language.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguage() {
            return language;
        }

        /**
         * D�finit la valeur de la propri�t� language.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguage(String value) {
            this.language = value;
        }

        /**
         * Obtient la valeur de la propri�t� wxrVersion.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getWxrVersion() {
            return wxrVersion;
        }

        /**
         * D�finit la valeur de la propri�t� wxrVersion.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setWxrVersion(BigDecimal value) {
            this.wxrVersion = value;
        }

        /**
         * Obtient la valeur de la propri�t� baseSiteUrl.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBaseSiteUrl() {
            return baseSiteUrl;
        }

        /**
         * D�finit la valeur de la propri�t� baseSiteUrl.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBaseSiteUrl(String value) {
            this.baseSiteUrl = value;
        }

        /**
         * Obtient la valeur de la propri�t� baseBlogUrl.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBaseBlogUrl() {
            return baseBlogUrl;
        }

        /**
         * D�finit la valeur de la propri�t� baseBlogUrl.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBaseBlogUrl(String value) {
            this.baseBlogUrl = value;
        }

        /**
         * Gets the value of the category property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the category property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCategory().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link bp.ovb2wp.wp.Category }
         * 
         * 
         */
        public List<bp.ovb2wp.wp.Category> getCategory() {
            if (category == null) {
                category = new ArrayList<bp.ovb2wp.wp.Category>();
            }
            return this.category;
        }

        /**
         * Gets the value of the tag property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tag property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTag().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tag }
         * 
         * 
         */
        public List<Tag> getTag() {
            if (tag == null) {
                tag = new ArrayList<Tag>();
            }
            return this.tag;
        }

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Rss.Channel.Item }
         * 
         * 
         */
        public List<Rss.Channel.Item> getItem() {
            if (item == null) {
                item = new ArrayList<Rss.Channel.Item>();
            }
            return this.item;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="link" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="pubDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element ref="{http://purl.org/dc/elements/1.1/}creator"/>
         *         &lt;element name="category" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                 &lt;attribute name="domain" type="{http://www.w3.org/2001/XMLSchema}string" />
         *                 &lt;attribute name="nicename" type="{http://www.w3.org/2001/XMLSchema}string" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="guid">
         *           &lt;complexType>
         *             &lt;simpleContent>
         *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
         *                 &lt;attribute name="isPermaLink" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
         *               &lt;/extension>
         *             &lt;/simpleContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element ref="{http://purl.org/rss/1.0/modules/content/}encoded"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}post_id"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}post_date"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}post_date_gmt"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}comment_status"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}ping_status"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}post_name"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}status"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}post_parent"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}menu_order"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}post_type"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}post_password"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}postmeta" maxOccurs="unbounded" minOccurs="0"/>
         *         &lt;element ref="{http://wordpress.org/export/1.0/}comment" maxOccurs="unbounded" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "title",
            "link",
            "pubDate",
            "creator",
            "category",
            "guid",
            "description",
            "encoded",
            "postId",
            "postDate",
            "postDateGmt",
            "commentStatus",
            "pingStatus",
            "postName",
            "status",
            "postParent",
            "menuOrder",
            "postType",
            "postPassword",
            "postmeta",
            "comment"
        })
        public static class Item {

            @XmlElement(namespace = "", required = true)
            protected String title;
            @XmlElement(namespace = "", required = true)
            protected String link;
            @XmlElement(namespace = "", required = true)
            protected String pubDate;
            @XmlElement(namespace = "http://purl.org/dc/elements/1.1/", required = true)
            protected String creator;
            @XmlElement(namespace = "")
            protected List<Rss.Channel.Item.Category> category;
            @XmlElement(namespace = "", required = true)
            protected Rss.Channel.Item.Guid guid;
            @XmlElement(namespace = "", required = true)
            protected Object description;
            @XmlElement(namespace = "http://purl.org/rss/1.0/modules/content/", required = true)
            protected String encoded;
            @XmlElement(name = "post_id")
            @XmlSchemaType(name = "unsignedByte")
            protected short postId;
            @XmlElement(name = "post_date", required = true)
            protected String postDate;
            @XmlElement(name = "post_date_gmt", required = true)
            protected String postDateGmt;
            @XmlElement(name = "comment_status", required = true)
            protected String commentStatus;
            @XmlElement(name = "ping_status", required = true)
            protected String pingStatus;
            @XmlElement(name = "post_name", required = true)
            protected String postName;
            @XmlElement(required = true)
            protected String status;
            @XmlElement(name = "post_parent")
            @XmlSchemaType(name = "unsignedByte")
            protected short postParent;
            @XmlElement(name = "menu_order")
            @XmlSchemaType(name = "unsignedByte")
            protected short menuOrder;
            @XmlElement(name = "post_type", required = true)
            protected String postType;
            @XmlElement(name = "post_password", required = true)
            protected Object postPassword;
            protected List<Postmeta> postmeta;
            protected List<Comment> comment;

            /**
             * Obtient la valeur de la propri�t� title.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTitle() {
                return title;
            }

            /**
             * D�finit la valeur de la propri�t� title.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTitle(String value) {
                this.title = value;
            }

            /**
             * Obtient la valeur de la propri�t� link.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLink() {
                return link;
            }

            /**
             * D�finit la valeur de la propri�t� link.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLink(String value) {
                this.link = value;
            }

            /**
             * Obtient la valeur de la propri�t� pubDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPubDate() {
                return pubDate;
            }

            /**
             * D�finit la valeur de la propri�t� pubDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPubDate(String value) {
                this.pubDate = value;
            }

            /**
             * Obtient la valeur de la propri�t� creator.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCreator() {
                return creator;
            }

            /**
             * D�finit la valeur de la propri�t� creator.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCreator(String value) {
                this.creator = value;
            }

            /**
             * Gets the value of the category property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the category property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getCategory().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Rss.Channel.Item.Category }
             * 
             * 
             */
            public List<Rss.Channel.Item.Category> getCategory() {
                if (category == null) {
                    category = new ArrayList<Rss.Channel.Item.Category>();
                }
                return this.category;
            }

            /**
             * Obtient la valeur de la propri�t� guid.
             * 
             * @return
             *     possible object is
             *     {@link Rss.Channel.Item.Guid }
             *     
             */
            public Rss.Channel.Item.Guid getGuid() {
                return guid;
            }

            /**
             * D�finit la valeur de la propri�t� guid.
             * 
             * @param value
             *     allowed object is
             *     {@link Rss.Channel.Item.Guid }
             *     
             */
            public void setGuid(Rss.Channel.Item.Guid value) {
                this.guid = value;
            }

            /**
             * Obtient la valeur de la propri�t� description.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getDescription() {
                return description;
            }

            /**
             * D�finit la valeur de la propri�t� description.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setDescription(Object value) {
                this.description = value;
            }

            /**
             * Obtient la valeur de la propri�t� encoded.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEncoded() {
                return encoded;
            }

            /**
             * D�finit la valeur de la propri�t� encoded.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEncoded(String value) {
                this.encoded = value;
            }

            /**
             * Obtient la valeur de la propri�t� postId.
             * 
             */
            public short getPostId() {
                return postId;
            }

            /**
             * D�finit la valeur de la propri�t� postId.
             * 
             */
            public void setPostId(short value) {
                this.postId = value;
            }

            /**
             * Obtient la valeur de la propri�t� postDate.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostDate() {
                return postDate;
            }

            /**
             * D�finit la valeur de la propri�t� postDate.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostDate(String value) {
                this.postDate = value;
            }

            /**
             * Obtient la valeur de la propri�t� postDateGmt.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostDateGmt() {
                return postDateGmt;
            }

            /**
             * D�finit la valeur de la propri�t� postDateGmt.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostDateGmt(String value) {
                this.postDateGmt = value;
            }

            /**
             * Obtient la valeur de la propri�t� commentStatus.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCommentStatus() {
                return commentStatus;
            }

            /**
             * D�finit la valeur de la propri�t� commentStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCommentStatus(String value) {
                this.commentStatus = value;
            }

            /**
             * Obtient la valeur de la propri�t� pingStatus.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPingStatus() {
                return pingStatus;
            }

            /**
             * D�finit la valeur de la propri�t� pingStatus.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPingStatus(String value) {
                this.pingStatus = value;
            }

            /**
             * Obtient la valeur de la propri�t� postName.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostName() {
                return postName;
            }

            /**
             * D�finit la valeur de la propri�t� postName.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostName(String value) {
                this.postName = value;
            }

            /**
             * Obtient la valeur de la propri�t� status.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStatus() {
                return status;
            }

            /**
             * D�finit la valeur de la propri�t� status.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStatus(String value) {
                this.status = value;
            }

            /**
             * Obtient la valeur de la propri�t� postParent.
             * 
             */
            public short getPostParent() {
                return postParent;
            }

            /**
             * D�finit la valeur de la propri�t� postParent.
             * 
             */
            public void setPostParent(short value) {
                this.postParent = value;
            }

            /**
             * Obtient la valeur de la propri�t� menuOrder.
             * 
             */
            public short getMenuOrder() {
                return menuOrder;
            }

            /**
             * D�finit la valeur de la propri�t� menuOrder.
             * 
             */
            public void setMenuOrder(short value) {
                this.menuOrder = value;
            }

            /**
             * Obtient la valeur de la propri�t� postType.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostType() {
                return postType;
            }

            /**
             * D�finit la valeur de la propri�t� postType.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostType(String value) {
                this.postType = value;
            }

            /**
             * Obtient la valeur de la propri�t� postPassword.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getPostPassword() {
                return postPassword;
            }

            /**
             * D�finit la valeur de la propri�t� postPassword.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setPostPassword(Object value) {
                this.postPassword = value;
            }

            /**
             * Gets the value of the postmeta property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the postmeta property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getPostmeta().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Postmeta }
             * 
             * 
             */
            public List<Postmeta> getPostmeta() {
                if (postmeta == null) {
                    postmeta = new ArrayList<Postmeta>();
                }
                return this.postmeta;
            }

            /**
             * Gets the value of the comment property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the comment property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getComment().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Comment }
             * 
             * 
             */
            public List<Comment> getComment() {
                if (comment == null) {
                    comment = new ArrayList<Comment>();
                }
                return this.comment;
            }


            /**
             * <p>Classe Java pour anonymous complex type.
             * 
             * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *       &lt;attribute name="domain" type="{http://www.w3.org/2001/XMLSchema}string" />
             *       &lt;attribute name="nicename" type="{http://www.w3.org/2001/XMLSchema}string" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Category {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "domain")
                protected String domain;
                @XmlAttribute(name = "nicename")
                protected String nicename;

                /**
                 * Obtient la valeur de la propri�t� value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * D�finit la valeur de la propri�t� value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtient la valeur de la propri�t� domain.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDomain() {
                    return domain;
                }

                /**
                 * D�finit la valeur de la propri�t� domain.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDomain(String value) {
                    this.domain = value;
                }

                /**
                 * Obtient la valeur de la propri�t� nicename.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNicename() {
                    return nicename;
                }

                /**
                 * D�finit la valeur de la propri�t� nicename.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNicename(String value) {
                    this.nicename = value;
                }

            }


            /**
             * <p>Classe Java pour anonymous complex type.
             * 
             * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;simpleContent>
             *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
             *       &lt;attribute name="isPermaLink" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
             *     &lt;/extension>
             *   &lt;/simpleContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "value"
            })
            public static class Guid {

                @XmlValue
                protected String value;
                @XmlAttribute(name = "isPermaLink", required = true)
                protected boolean isPermaLink;

                /**
                 * Obtient la valeur de la propri�t� value.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getValue() {
                    return value;
                }

                /**
                 * D�finit la valeur de la propri�t� value.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setValue(String value) {
                    this.value = value;
                }

                /**
                 * Obtient la valeur de la propri�t� isPermaLink.
                 * 
                 */
                public boolean isIsPermaLink() {
                    return isPermaLink;
                }

                /**
                 * D�finit la valeur de la propri�t� isPermaLink.
                 * 
                 */
                public void setIsPermaLink(boolean value) {
                    this.isPermaLink = value;
                }

            }

        }

    }

}
