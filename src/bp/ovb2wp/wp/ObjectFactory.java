//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.7 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2014.02.06 � 12:04:00 AM CET 
//


package bp.ovb2wp.wp;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the bp.ovb2wp.wp package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BaseSiteUrl_QNAME = new QName("http://wordpress.org/export/1.0/", "base_site_url");
    private final static QName _CommentStatus_QNAME = new QName("http://wordpress.org/export/1.0/", "comment_status");
    private final static QName _Encoded_QNAME = new QName("http://purl.org/rss/1.0/modules/content/", "encoded");
    private final static QName _Status_QNAME = new QName("http://wordpress.org/export/1.0/", "status");
    private final static QName _Creator_QNAME = new QName("http://purl.org/dc/elements/1.1/", "creator");
    private final static QName _WxrVersion_QNAME = new QName("http://wordpress.org/export/1.0/", "wxr_version");
    private final static QName _BaseBlogUrl_QNAME = new QName("http://wordpress.org/export/1.0/", "base_blog_url");
    private final static QName _PostId_QNAME = new QName("http://wordpress.org/export/1.0/", "post_id");
    private final static QName _PostParent_QNAME = new QName("http://wordpress.org/export/1.0/", "post_parent");
    private final static QName _MenuOrder_QNAME = new QName("http://wordpress.org/export/1.0/", "menu_order");
    private final static QName _PostDateGmt_QNAME = new QName("http://wordpress.org/export/1.0/", "post_date_gmt");
    private final static QName _PostPassword_QNAME = new QName("http://wordpress.org/export/1.0/", "post_password");
    private final static QName _PostDate_QNAME = new QName("http://wordpress.org/export/1.0/", "post_date");
    private final static QName _PostName_QNAME = new QName("http://wordpress.org/export/1.0/", "post_name");
    private final static QName _PingStatus_QNAME = new QName("http://wordpress.org/export/1.0/", "ping_status");
    private final static QName _PostType_QNAME = new QName("http://wordpress.org/export/1.0/", "post_type");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: bp.ovb2wp.wp
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Rss }
     * 
     */
    public Rss createRss() {
        return new Rss();
    }

    /**
     * Create an instance of {@link Rss.Channel }
     * 
     */
    public Rss.Channel createRssChannel() {
        return new Rss.Channel();
    }

    /**
     * Create an instance of {@link Rss.Channel.Item }
     * 
     */
    public Rss.Channel.Item createRssChannelItem() {
        return new Rss.Channel.Item();
    }

    /**
     * Create an instance of {@link Postmeta }
     * 
     */
    public Postmeta createPostmeta() {
        return new Postmeta();
    }

    /**
     * Create an instance of {@link Tag }
     * 
     */
    public Tag createTag() {
        return new Tag();
    }

    /**
     * Create an instance of {@link bp.ovb2wp.wp.Category }
     * 
     */
    public bp.ovb2wp.wp.Category createCategory() {
        return new bp.ovb2wp.wp.Category();
    }

    /**
     * Create an instance of {@link Comment }
     * 
     */
    public Comment createComment() {
        return new Comment();
    }

    /**
     * Create an instance of {@link Rss.Channel.Item.Category }
     * 
     */
    public Rss.Channel.Item.Category createRssChannelItemCategory() {
        return new Rss.Channel.Item.Category();
    }

    /**
     * Create an instance of {@link Rss.Channel.Item.Guid }
     * 
     */
    public Rss.Channel.Item.Guid createRssChannelItemGuid() {
        return new Rss.Channel.Item.Guid();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "base_site_url")
    public JAXBElement<String> createBaseSiteUrl(String value) {
        return new JAXBElement<String>(_BaseSiteUrl_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "comment_status")
    public JAXBElement<String> createCommentStatus(String value) {
        return new JAXBElement<String>(_CommentStatus_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://purl.org/rss/1.0/modules/content/", name = "encoded")
    public JAXBElement<String> createEncoded(String value) {
        return new JAXBElement<String>(_Encoded_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "status")
    public JAXBElement<String> createStatus(String value) {
        return new JAXBElement<String>(_Status_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://purl.org/dc/elements/1.1/", name = "creator")
    public JAXBElement<String> createCreator(String value) {
        return new JAXBElement<String>(_Creator_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "wxr_version")
    public JAXBElement<BigDecimal> createWxrVersion(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_WxrVersion_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "base_blog_url")
    public JAXBElement<String> createBaseBlogUrl(String value) {
        return new JAXBElement<String>(_BaseBlogUrl_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "post_id")
    public JAXBElement<Short> createPostId(Short value) {
        return new JAXBElement<Short>(_PostId_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "post_parent")
    public JAXBElement<Short> createPostParent(Short value) {
        return new JAXBElement<Short>(_PostParent_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "menu_order")
    public JAXBElement<Short> createMenuOrder(Short value) {
        return new JAXBElement<Short>(_MenuOrder_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "post_date_gmt")
    public JAXBElement<String> createPostDateGmt(String value) {
        return new JAXBElement<String>(_PostDateGmt_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "post_password")
    public JAXBElement<Object> createPostPassword(Object value) {
        return new JAXBElement<Object>(_PostPassword_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "post_date")
    public JAXBElement<String> createPostDate(String value) {
        return new JAXBElement<String>(_PostDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "post_name")
    public JAXBElement<String> createPostName(String value) {
        return new JAXBElement<String>(_PostName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "ping_status")
    public JAXBElement<String> createPingStatus(String value) {
        return new JAXBElement<String>(_PingStatus_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://wordpress.org/export/1.0/", name = "post_type")
    public JAXBElement<String> createPostType(String value) {
        return new JAXBElement<String>(_PostType_QNAME, String.class, null, value);
    }

}
