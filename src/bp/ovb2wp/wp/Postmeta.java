//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.7 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2014.02.06 � 12:04:00 AM CET 
//


package bp.ovb2wp.wp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="meta_key" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="meta_value" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "metaKey",
    "metaValue"
})
@XmlRootElement(name = "postmeta")
public class Postmeta {

    @XmlElement(name = "meta_key", required = true)
    protected String metaKey;
    @XmlElement(name = "meta_value")
    @XmlSchemaType(name = "unsignedInt")
    protected long metaValue;

    /**
     * Obtient la valeur de la propri�t� metaKey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetaKey() {
        return metaKey;
    }

    /**
     * D�finit la valeur de la propri�t� metaKey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetaKey(String value) {
        this.metaKey = value;
    }

    /**
     * Obtient la valeur de la propri�t� metaValue.
     * 
     */
    public long getMetaValue() {
        return metaValue;
    }

    /**
     * D�finit la valeur de la propri�t� metaValue.
     * 
     */
    public void setMetaValue(long value) {
        this.metaValue = value;
    }

}
