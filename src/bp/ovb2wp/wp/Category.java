//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.7 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2014.02.06 � 12:04:00 AM CET 
//


package bp.ovb2wp.wp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="category_nicename" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="category_parent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cat_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "categoryNicename",
    "categoryParent",
    "catName"
})
@XmlRootElement(name = "category")
public class Category {

    @XmlElement(name = "category_nicename", required = true)
    protected String categoryNicename;
    @XmlElement(name = "category_parent", required = true)
    protected String categoryParent;
    @XmlElement(name = "cat_name", required = true)
    protected String catName;

    /**
     * Obtient la valeur de la propri�t� categoryNicename.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryNicename() {
        return categoryNicename;
    }

    /**
     * D�finit la valeur de la propri�t� categoryNicename.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryNicename(String value) {
        this.categoryNicename = value;
    }

    /**
     * Obtient la valeur de la propri�t� categoryParent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryParent() {
        return categoryParent;
    }

    /**
     * D�finit la valeur de la propri�t� categoryParent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryParent(String value) {
        this.categoryParent = value;
    }

    /**
     * Obtient la valeur de la propri�t� catName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatName() {
        return catName;
    }

    /**
     * D�finit la valeur de la propri�t� catName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatName(String value) {
        this.catName = value;
    }

}
