package bp.ovb2wp;

import java.util.regex.Pattern;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
 
/**
 * Implementation which is able to decide to use a CDATA section for a string.
 */
public class CDataXMLStreamWriter extends DelegatingXMLStreamWriter
{
   private static final Pattern XML_CHARS = Pattern.compile( "[&<>]" );
 
   public CDataXMLStreamWriter( XMLStreamWriter del )
   {
      super( del );
   }
 
   /*@Override
   public void writeCharacters( String text ) throws XMLStreamException
   {
	  // text=text.replace("�","&eacute;");
	//	text=text.replace("�","&egrave;");
	//	text=text.replace("�","&ecirc;");
		//text=text.replace("�","&euml;");
		//text=text.replace("'","&#145;");
	   
	  
	   
      boolean useCData = XML_CHARS.matcher( text ).find();
      if( useCData )
      {
         super.writeCData( text );
      }
      else
      {
         super.writeCharacters( text );
      }
   }*/
}
